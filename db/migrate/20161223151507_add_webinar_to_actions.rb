class AddWebinarToActions < ActiveRecord::Migration[5.0]
  def change
  	add_column :zoom_user_actions, :zoom_webinar_id, :string
  end
end
