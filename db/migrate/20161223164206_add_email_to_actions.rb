class AddEmailToActions < ActiveRecord::Migration[5.0]
  def change
  	add_column :zoom_user_actions, :zoom_user_email, :string
  end
end
