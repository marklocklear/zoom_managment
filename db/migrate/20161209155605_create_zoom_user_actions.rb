class CreateZoomUserActions < ActiveRecord::Migration[5.0]
  def change
    create_table :zoom_user_actions do |t|
      t.string :zoom_user_name
      t.string :zoom_user_id
      t.string :action_date
      t.boolean :large_meeting
      t.text :notes

      t.timestamps
    end
  end
end
