class CreateWebinarRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :webinar_requests do |t|
      t.string :name
      t.string :email
      t.string :webinar_date
      t.string :webinar_time
      t.string :duration
      t.string :topic
      t.text :agenda
      t.string :panelist
      t.string :learn_event

      t.timestamps
    end
  end
end
