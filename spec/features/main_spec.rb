require "rails_helper"
feature "Accounts" do
	scenario "creating an account and sign in" do
		# u = User.create! email: 'blah@blah.com', authorized: 'true', password: 'password', password_confirmation: 'password'
		visit root_path
		expect(page).to have_content("Sign up")
		fill_in "Email", with: "blah@blah.com"
		fill_in "Password", with: "password"
		click_button "Log in"
		expect(page).to have_content("All Webinars")
		#test recurring webinars
		visit recurring_webinars_path
		expect(page).to have_content("All Bugs Good and Bad")
		#test webinar requests; TODO need a better test here
		visit webinar_requests_path
		expect(page).to have_content("Webinar")
		#TODO create test for /actions page

		#test users page
		visit zoom_users_path
		expect(page).to have_content("Heemstra")
		#add test to toggle user type and verify change
	end
end
