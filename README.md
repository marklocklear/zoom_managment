# README

A Rails app that allows me to manage zoom webinars via the Zoom API.

Things you may want to cover:

* Root (/webinars) shows upcoming webinars

* Actions (/zoom_user_actions) will promote a basic user to pro for 24hrs based on
	the promotion(action) date. If a webinar id is entered the that user will also be
	set as an Alternative Host for that webinar. Email notifications are sent when an
	action is created, and also on the day of an event; see lib/tasks/user_actions.rake

* Run tests with 'rake spec'; see spec/features/main_spec.rb

* ...
