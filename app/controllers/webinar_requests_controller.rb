class WebinarRequestsController < ApplicationController
  before_action :set_webinar_request, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, :authorized, :except => [:not_authorized, :create]
  skip_before_action :verify_authenticity_token, :only => [:create]

  # GET /webinar_requests
  # GET /webinar_requests.json
  def index
    @webinar_requests = WebinarRequest.all.order created_at: :desc
  end

  # GET /webinar_requests/1
  # GET /webinar_requests/1.json
  def show
  end

  # GET /webinar_requests/new
  def new
    @webinar_request = WebinarRequest.new
  end

  # GET /webinar_requests/1/edit
  def edit
  end

  # POST /webinar_requests
  # POST /webinar_requests.json
  def create
    @webinar_request = WebinarRequest.new(webinar_request_params)

    respond_to do |format|
      if @webinar_request.save
        format.html { redirect_to @webinar_request, notice: 'Webinar request was successfully created.' }
        format.json { render :show, status: :created, location: @webinar_request }
      else
        format.html { render :new }
        format.json { render json: @webinar_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /webinar_requests/1
  # PATCH/PUT /webinar_requests/1.json
  def update
    respond_to do |format|
      if @webinar_request.update(webinar_request_params)
        format.html { redirect_to @webinar_request, notice: 'Webinar request was successfully updated.' }
        format.json { render :show, status: :ok, location: @webinar_request }
      else
        format.html { render :edit }
        format.json { render json: @webinar_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /webinar_requests/1
  # DELETE /webinar_requests/1.json
  def destroy
    @webinar_request.destroy
    respond_to do |format|
      format.html { redirect_to webinar_requests_url, notice: 'Webinar request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_webinar_request
      @webinar_request = WebinarRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def webinar_request_params
      params.require(:webinar_request).permit(
                      :name,
                      :email,
                      :webinar_date,
                      :webinar_time,
                      :duration,
                      :topic,
                      :agenda,
                      :panelist,
                      :learn_event)

    end
end
