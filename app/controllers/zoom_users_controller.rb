class ZoomUsersController < ApplicationController
	before_action :set_zoom_api_keys, :get_users
	before_action :authenticate_user!, :authorized, :except => [:not_authorized]

	def get_users
		@zoom_options[:body][:page_size] = 100
    zoom_users = HTTParty.post("https://api.zoom.us/v1/user/list", @zoom_options)
		@zoom_users = zoom_users.parsed_response["users"]
		@pro_users = @zoom_users.select { |user| user['type'] == 2 }
		@basic_users = @zoom_users.select { |user| user['type'] == 1 }
	end

	def toggle_user_type
		@zoom_options[:body][:id] = params[:zoom_user_id]
		user = HTTParty.post("https://api.zoom.us/v1/user/get", @zoom_options)
		if user['type'] == 2
			@zoom_options[:body][:type] = 1
		else
			@zoom_options[:body][:type] = 2
		end
		toggle = HTTParty.post("https://api.zoom.us/v1/user/update", @zoom_options)
		redirect_to zoom_users_path
	end

end
