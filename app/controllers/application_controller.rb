class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :set_zoom_api_keys, :authorized

  def authorized
    if !current_user.authorized?
      redirect_to not_authorized_path
    end
  end

  def set_zoom_api_keys
    @zoom_options = {
      body: {
        api_key: Figaro.env.zoom_api_key,
        api_secret: Figaro.env.zoom_api_secret,
        host_id: Figaro.env.zoom_host_id,
        data_type: 'JSON'
      }
    }
  end
end
