class ZoomUserActionsController < ApplicationController
  before_action :set_zoom_user_action, only: [:show, :edit, :update, :destroy]
  before_action :set_zoom_api_keys
  before_action :authenticate_user!, :authorized, :except => [:not_authorized, :demote_user, :promote_user]
  skip_before_action :verify_authenticity_token, :only => [:demote_user, :promote_user]

  # GET /zoom_user_actions
  # GET /zoom_user_actions.json
  def index
    @zoom_user_actions = ZoomUserAction.all.order action_date: :desc
  end

  # GET /zoom_user_actions/1
  # GET /zoom_user_actions/1.json
  def show
  end

  # GET /zoom_user_actions/new
  def new
    @zoom_options[:body][:page_size] = 100
    zoom_users = HTTParty.post("https://api.zoom.us/v1/user/list", @zoom_options)
    @zoom_users = zoom_users.parsed_response["users"]
    @pro_users = @zoom_users.select { |user| user['type'] == 2 }
    @basic_users = @zoom_users.select { |user| user['type'] == 1 }
    @zoom_user_action = ZoomUserAction.new
  end

  # GET /zoom_user_actions/1/edit
  def edit
  end

  # POST /zoom_user_actions
  # POST /zoom_user_actions.json
  def create
    @zoom_user_action = ZoomUserAction.new(zoom_user_action_params)
    @zoom_options[:body][:id] = zoom_user_action_params['zoom_user_id']
    user = HTTParty.post("https://api.zoom.us/v1/user/get", @zoom_options)
    @zoom_user_action.zoom_user_name = user['first_name'] + ' ' + user['last_name']
    @zoom_user_action.zoom_user_email = user['email']

    if params[:email_user] == 'true'
      ZoomUserActionsMailer.add_user_action(@zoom_user_action).deliver
    end

    respond_to do |format|
      if @zoom_user_action.save
        format.html { redirect_to @zoom_user_action, notice: 'Zoom user action was successfully created.' }
        format.json { render :show, status: :created, location: @zoom_user_action }
      else
        format.html { render :new }
        format.json { render json: @zoom_user_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /zoom_user_actions/1
  # PATCH/PUT /zoom_user_actions/1.json
  def update
    respond_to do |format|
      if @zoom_user_action.update(zoom_user_action_params)
        format.html { redirect_to @zoom_user_action, notice: 'Zoom user action was successfully updated.' }
        format.json { render :show, status: :ok, location: @zoom_user_action }
      else
        format.html { render :edit }
        format.json { render json: @zoom_user_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /zoom_user_actions/1
  # DELETE /zoom_user_actions/1.json
  def destroy
    @zoom_user_action.destroy
    respond_to do |format|
      format.html { redirect_to zoom_user_actions_url, notice: 'Zoom user action was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def demote_user
    @zoom_options[:body][:id] = params[:zoom_user_id]
    @zoom_options[:body][:type] = 1
    HTTParty.post("https://api.zoom.us/v1/user/update", @zoom_options)
  end

  def promote_user
    @zoom_options[:body][:id] = params[:zoom_user_id]
    @zoom_options[:body][:type] = 2
    if params[:large_meeting] == "true"
      @zoom_options[:body][:enable_large] = true
    end
    @zoom_options[:body][:zoom_user_email] = params[:zoom_user_email]
    
    HTTParty.post("https://api.zoom.us/v1/user/update", @zoom_options)
    #if its a webinar in addtion to making user pro, also set Alternative Host
    if !params[:zoom_webinar_id].empty?
      @zoom_options[:body][:zoom_webinar_id] = params[:zoom_webinar_id] #used by mailer
      @zoom_options[:body][:id] = params[:zoom_webinar_id]
      @zoom_options[:body][:option_alternative_hosts] = params[:zoom_user_email]
      HTTParty.post("https://api.zoom.us/v1/webinar/update", @zoom_options)
    end
    ZoomUserActionsMailer.promote_user_action(@zoom_options).deliver
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_zoom_user_action
      @zoom_user_action = ZoomUserAction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def zoom_user_action_params
      params.require(:zoom_user_action).permit(:zoom_user_id,
                                               :zoom_webinar_id,
                                               :zoom_user_name,
                                               :zoom_user_email,
                                               :action_date,
                                               :large_meeting,
                                               :notes)
    end
end
