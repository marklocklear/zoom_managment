class WebinarsController < ApplicationController
	before_action :set_zoom_api_keys
	before_action :authenticate_user!, :authorized, :except => [:not_authorized]

	def index
		@registration_webinars = HTTParty.post("https://api.zoom.us/v1/webinar/list/registration", @zoom_options)
		@recurring_webinars = HTTParty.post("https://api.zoom.us/v1/webinar/list/", @zoom_options)
		@webinars = @registration_webinars.parsed_response["webinars"] + @recurring_webinars.parsed_response["webinars"]
	end

	def recurring_webinars
		recurring_webinars = HTTParty.post("https://api.zoom.us/v1/webinar/list/", @zoom_options)
		@recurring_webinars = recurring_webinars.parsed_response["webinars"]
	end

	def registration_webinars
		registration_webinars = HTTParty.post("https://api.zoom.us/v1/webinar/list/registration", @zoom_options)
		@registration_webinars = registration_webinars.parsed_response["webinars"]
	end

	def create_webinar
		offset = params[:start_time][0...2].to_i + 4 #add 4 hr offset
		new_time = params[:start_time].gsub(/^../, offset.to_s) #update first to characters with new time
		iso_start_time = params[:start_date] + "T" + new_time + ":00Z"
		@zoom_options[:body][:topic] = params[:topic]
		@zoom_options[:body][:start_time] = iso_start_time
		@zoom_options[:body][:timezone] = "America/New_York"
		@zoom_options[:body][:type] = 5
		@zoom_options[:body][:approval_type] = 0
		@zoom_options[:body][:duration] = 60
		# @zoom_options[:body][:option_alternative_hosts] = "marklocklear@extension.org"
		HTTParty.post("https://api.zoom.us/v1/webinar/create", @zoom_options)
		flash[:info] = "Successfully Created Webinar"
		redirect_to webinar_requests_path
	end

end
