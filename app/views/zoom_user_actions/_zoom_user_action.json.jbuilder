json.extract! zoom_user_action, :id, :zoom_user, :action_date, :large_meeting, :notes, :created_at, :updated_at
json.url zoom_user_action_url(zoom_user_action, format: :json)