class ZoomUserActionsMailer < ActionMailer::Base
	default :from => 'marklocklear@extension.org'

	def add_user_action(action_params)
		@action_params = action_params
		mail(to: @action_params[:zoom_user_email],
				 cc: 'marklocklear@extension.org',
				 subject: 'Your eXtension Zoom Event has been Scheduled')
	end

	def promote_user_action(action_params)
		@action_params = action_params
		mail(to: @action_params[:body][:zoom_user_email],
  			 cc: 'marklocklear@extension.org',
				 subject: 'Your eXtension Zoom Event is Today')
	end
end