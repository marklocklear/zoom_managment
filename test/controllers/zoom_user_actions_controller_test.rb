require 'test_helper'

class ZoomUserActionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @zoom_user_action = zoom_user_actions(:one)
  end

  test "should get index" do
    get zoom_user_actions_url
    assert_response :success
  end

  test "should get new" do
    get new_zoom_user_action_url
    assert_response :success
  end

  test "should create zoom_user_action" do
    assert_difference('ZoomUserAction.count') do
      post zoom_user_actions_url, params: { zoom_user_action: { action_date: @zoom_user_action.action_date, large_meeting: @zoom_user_action.large_meeting, notes: @zoom_user_action.notes, zoom_user: @zoom_user_action.zoom_user } }
    end

    assert_redirected_to zoom_user_action_url(ZoomUserAction.last)
  end

  test "should show zoom_user_action" do
    get zoom_user_action_url(@zoom_user_action)
    assert_response :success
  end

  test "should get edit" do
    get edit_zoom_user_action_url(@zoom_user_action)
    assert_response :success
  end

  test "should update zoom_user_action" do
    patch zoom_user_action_url(@zoom_user_action), params: { zoom_user_action: { action_date: @zoom_user_action.action_date, large_meeting: @zoom_user_action.large_meeting, notes: @zoom_user_action.notes, zoom_user: @zoom_user_action.zoom_user } }
    assert_redirected_to zoom_user_action_url(@zoom_user_action)
  end

  test "should destroy zoom_user_action" do
    assert_difference('ZoomUserAction.count', -1) do
      delete zoom_user_action_url(@zoom_user_action)
    end

    assert_redirected_to zoom_user_actions_url
  end
end
