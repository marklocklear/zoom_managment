require 'test_helper'

class WebinarRequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @webinar_request = webinar_requests(:one)
  end

  test "should get index" do
    get webinar_requests_url
    assert_response :success
  end

  test "should get new" do
    get new_webinar_request_url
    assert_response :success
  end

  test "should create webinar_request" do
    assert_difference('WebinarRequest.count') do
      post webinar_requests_url, params: { webinar_request: { agenda: @webinar_request.agenda, duration: @webinar_request.duration, email: @webinar_request.email, name: @webinar_request.name, panelist: @webinar_request.panelist, topic: @webinar_request.topic, webinar_date: @webinar_request.webinar_date, webinar_time: @webinar_request.webinar_time } }
    end

    assert_redirected_to webinar_request_url(WebinarRequest.last)
  end

  test "should show webinar_request" do
    get webinar_request_url(@webinar_request)
    assert_response :success
  end

  test "should get edit" do
    get edit_webinar_request_url(@webinar_request)
    assert_response :success
  end

  test "should update webinar_request" do
    patch webinar_request_url(@webinar_request), params: { webinar_request: { agenda: @webinar_request.agenda, duration: @webinar_request.duration, email: @webinar_request.email, name: @webinar_request.name, panelist: @webinar_request.panelist, topic: @webinar_request.topic, webinar_date: @webinar_request.webinar_date, webinar_time: @webinar_request.webinar_time } }
    assert_redirected_to webinar_request_url(@webinar_request)
  end

  test "should destroy webinar_request" do
    assert_difference('WebinarRequest.count', -1) do
      delete webinar_request_url(@webinar_request)
    end

    assert_redirected_to webinar_requests_url
  end
end
