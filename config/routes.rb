Rails.application.routes.draw do
  resources :zoom_user_actions
  resources :webinar_requests
  devise_for :users
  get 'zoom_users' => 'zoom_users#index'
  get 'pro_users' => 'zoom_users#pro_users'
  get 'basic_users' => 'zoom_users#basic_users'
  get 'webinars' => 'webinars#index'
  get 'recurring_webinars' => 'webinars#recurring_webinars'
  get 'registration_webinars' => 'webinars#registration_webinars'
  post 'create_webinar' => 'webinars#create_webinar'
  post 'toggle_user_type' => 'zoom_users#toggle_user_type'
  post 'demote_user' => 'zoom_user_actions#demote_user'
  post 'promote_user' => 'zoom_user_actions#promote_user'
  get 'zoom_action_logs' => 'zoom_user_actions#logs'
  get 'not_authorized' => 'webinars#not_authorized'

  root to: 'webinars#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
