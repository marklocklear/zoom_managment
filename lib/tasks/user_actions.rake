desc "This task is called by the Heroku scheduler add-on"
task :demote_users => :environment do
	session = ActionDispatch::Integration::Session.new(Rails.application)
  actions = ZoomUserAction.where action_date: Time.now.yesterday.strftime("%m/%d/%Y")
  actions.each do |a|
  	session.post "/demote_user", {zoom_user_id: a.zoom_user_id}
  end
end

task :promote_users => :environment do
	session = ActionDispatch::Integration::Session.new(Rails.application)
  actions = ZoomUserAction.where action_date: Time.now.strftime("%m/%d/%Y")
  actions.each do |a|
  	session.post "/promote_user", {zoom_user_id: a.zoom_user_id,
                                   zoom_webinar_id: a.zoom_webinar_id,
                                   zoom_user_email: a.zoom_user_email,
                                   large_meeting: a.large_meeting}
  end
end